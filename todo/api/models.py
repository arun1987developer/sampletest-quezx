from django.db import models

choice_status = (('todo','TODO'),('inprogress','In Progress'),('done','Done'))


class Todo(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100, blank=True, default='')
    description = models.TextField()
    state = models.CharField(choices=choice_status,max_length=20,default='todo')
    due_date = models.DateField()

    class Meta:
        ordering = ('created',)

    def __str__(self):
        return self.title
