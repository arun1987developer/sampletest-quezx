from rest_framework import generics
from .models import Todo
from .serializers import TodoSerializer
from django.http import HttpResponse
from django.core import serializers
from datetime import datetime
from django.http import JsonResponse

class TodoList(generics.ListCreateAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer

class TodoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer

def todofilterstatus(request,value):

    if '-' in value:
        year, month, day = value.split('-')
        try:
            newDate = datetime(int(year),int(month),int(day))
            correctDate = True
        except ValueError:
            return JsonResponse({'Error':'Wrong Date format please use yyyy-mm-dd'})

        queryset = Todo.objects.filter(due_date=value)
    else:
        queryset = Todo.objects.filter(state=value)


    response = serializers.serialize('json', queryset)
    return HttpResponse(response)




