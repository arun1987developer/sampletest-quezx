from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    path('', views.TodoList.as_view()),
    path('todos/', views.TodoList.as_view()),
    path('todo/<int:pk>/',views.TodoDetail.as_view()),
    path('todos/filter/<slug:value>/', views.todofilterstatus),
]

urlpatterns = format_suffix_patterns(urlpatterns)
