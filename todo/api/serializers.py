from rest_framework import serializers
from .models import Todo, choice_status


class TodoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Todo
        fields = ('id', 'title', 'description', 'due_date', 'state','created')
