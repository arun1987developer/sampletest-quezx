# Project Title

Todo Sample API

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

What things you need to install the software

```
Python3
Depending on the OS platform you are using please follow the instructions from https://realpython.com/installing-python/

Pipenv
To install the Pipenv please go through the instructions from https://pipenv.readthedocs.io/en/latest/

```

### Installing

A step by step series to tell you how to get a development env running
Once you have both the softwares installed
Please download the files from repo
And on the root of the folder
Execute the command
```
pipenv install

```

Once all the dependencies are installed. Start the environment using the following command
```
pipenv shell
```


Go to the folder todo using the command

```
cd todo
```

Once you are in the folder todo
Run the following command
```
python3 manage.py runserver
```

This will start the dev server http://127.0.0.1:8000/

## Running the tests

Automated test will run through the shell script test-script.sh

```
./test-script.sh
```

This automated test will add two todos and show you the todos added via REST API services (filter by todo and date also)
To Delete and Update I have added the curl commands below please use that for testing

Please remember json format will be
```
{
    "title": "",
    "description": "",
    "due_date": 'YYYY-MM-DD',
    "state": 'todo|inprogress|done'
}
```

For ADD
```
curl --header "Content-Type: application/json" --request POST --data '<json-format>' 'http://127.0.0.1:8000/todos/'
```
For DELETE
```
curl --header "Content-Type: application/json" --request DELETE http://127.0.0.1:8000/todo/<id of the TODO>/
```
For Update
```
curl --header "Content-Type: application/json" --request PUT --data <Json-data> http://127.0.0.1:8000/todo/<id of the TODO>/

```

Filters you can use with postman or terminal

Filter using state
```
curl http://127.0.0.1:8000/todos/filter/<state>/ | json_pp
```
State can be todo | inprogress | done

Filter using due_date
```
curl http://127.0.0.1:8000/todos/filter/<date>/ | json_pp
```
Date format will be (YYY-MM-DD)




