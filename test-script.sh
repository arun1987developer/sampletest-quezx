#!/bin/bash
CT="Content-Type:application/json"
echo "Adding some todos using the curl post method http://127.0.0.1:8000/todos/ with json data"
addtodo=$(curl --header "Content-Type: application/json" --request POST --data '{"title":"Final task","description":"This is the Final testing of curl","due_date":"2018-12-28","state":"inprogress"}' 'http://127.0.0.1:8000/todos/')
echo ${addtodo}
addtodo=$(curl --header "Content-Type: application/json" --request POST --data '{"title":"Final task","description":"This is the Final testing of curl","due_date":"2018-12-23","state":"done"}' 'http://127.0.0.1:8000/todos/')
echo ${addtodo}

echo "Getting all the todos using the curl http://127.0.0.1:8000/todos/"

gettodos=$(curl -s 'http://127.0.0.1:8000/todos/')
echo ${gettodos} | python3 -mjson.tool

echo 'Getting all the todos using the filter state inprogress'
gettodos=$(curl -s 'http://127.0.0.1:8000/todos/filter/inprogress/')
echo ${gettodos} | python3 -mjson.tool

echo 'Getting all the todos using the filter due_date filter'
gettodos=$(curl -s 'http://127.0.0.1:8000/todos/filter/2018-12-23/')
echo ${gettodos} | python3 -mjson.tool

